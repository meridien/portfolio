{---------------------------------------------------------------------------------------------------
 Unit Name: BillingDataModule
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit BillingDataModule;

interface

uses
  SysUtils, Classes, DB, Dialogs, ADODB, BillingRules, Events;

type
  TDataModuleBilling = class(TDataModule)
    ADOConnTopBilling: TADOConnection;
    tblUnbilledEvents: TADOTable;
    qryUnbilledEvents: TADOQuery;
    qryBilledEvents:   TADOQuery;
    qryRules:          TADOQuery;
    spGenerateBill:    TADOStoredProc;
    qryRuleCategory:   TADOQuery;
    tblRules:          TADOTable;
    tblRuleCategory:   TADOTable;
    tblCustomers:      TADOTable;
    qryCustomerRules:  TADOQuery;
    tblRulesRuleID:    TSmallintField;
    tblRulesRuleCategoryID: TSmallintField;
    tblRulesDescription: TWideStringField;
    tblRulesPriority:  TIntegerField;
    tblRulesUnitRate:  TBCDField;
    tblRulesDiscount:  TFloatField;
    tblRulesMinParam:  TWideStringField;
    tblRulesMaxParam:  TWideStringField;
    qryCustomerRulesCustomerID: TSmallintField;
    qryCustomerRulesRuleID: TSmallintField;
    qryCustomerRulesPriority: TIntegerField;
    qryCustomerRulesRuleCategoryID: TSmallintField;
    qryCustomerRulesDescription: TWideStringField;
    qryCustomerRulesUnitRate: TBCDField;
    qryCustomerRulesDiscount: TFloatField;
    qryCustomerRulesMinParam: TWideStringField;
    qryCustomerRulesMaxParam: TWideStringField;
    spRestageTestData: TADOStoredProc;
    tblCustomerRule: TADOTable;
    tblCustomersCustomerID: TSmallintField;
    tblCustomersFirstName: TWideStringField;
    tblCustomersLastName: TWideStringField;
    qryUnbilledEventsCustomer: TWideStringField;
    qryUnbilledEventsUnits: TFloatField;
    qryUnbilledEventsTimeOccurred: TDateTimeField;
    qryUnbilledEventsCustomerID: TSmallintField;
    qryUnbilledEventsEventID: TSmallintField;
    qryBilledEventsCustomerID: TSmallintField;
    qryBilledEventsCustomer: TWideStringField;
    qryBilledEventsUnits: TFloatField;
    qryBilledEventsTimeOccurred: TDateTimeField;
    qryBilledEventsAmount: TBCDField;
    qryBilledEventsDescription: TWideStringField;
    qryBilledEventsUnitRate: TBCDField;
    qryBilledEventsDiscount: TFloatField;
    qryBilledEventsEventID: TSmallintField;
    qryBilledEventsruleid: TSmallintField;
    procedure qryUnbilledEventsAfterScroll(DataSet: TDataSet);
    procedure qryBilledEventsAfterScroll(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure tblRulesAfterPost(DataSet: TDataSet);
    procedure tblUnbilledEventsAfterPost(DataSet: TDataSet);
    procedure tblCustomerRuleAfterPost(DataSet: TDataSet);
  private
    procedure FilterRulesByCustomer(ACustomerID: string);
  public
    function GenerateBill(ARule: TBillingRule; AEvent: TBillingEvent): boolean;
    function RestageTestData: Boolean;
    procedure RefreshQueries;
    function Connect(ConnectionString: string): Boolean;
    procedure Disconnect;
  end;

var
  DataModuleBilling: TDataModuleBilling;

implementation

{$R *.dfm}

function TDataModuleBilling.Connect(ConnectionString: string): Boolean;
begin
  Result := False;
  try
    ADOConnTopBilling.ConnectionString := ConnectionString;
    if ConnectionString.IsEmpty then
      raise Exception.Create('Connection string cannot be empty');
    ADOConnTopBilling.Connected := True;
    Result := ADOConnTopBilling.Connected;
  except
    on E:EDatabaseError do
      ShowMessage(E.Message);
  end;

  qryUnbilledEvents.Open;
  qryBilledEvents.Open;
  qryRules.Open;
  qryCustomerRules.Open;
  tblCustomers.Open;
  tblCustomerRule.Open;
end;

procedure TDataModuleBilling.Disconnect;
begin
  qryUnbilledEvents.Close;
  qryBilledEvents.Close;
  qryRules.Close;
  qryCustomerRules.Close;
  tblCustomers.Close;
  tblCustomerRule.Close;

  if ADOConnTopBilling.Connected then
    ADOConnTopBilling.Connected := False;
end;

procedure TDataModuleBilling.DataModuleDestroy(Sender: TObject);
begin
  Disconnect;
end;

function TDataModuleBilling.GenerateBill(ARule: TBillingRule;
  AEvent: TBillingEvent): boolean;
begin
  Result := False;
  try
    spGenerateBill.Close;
    spGenerateBill.Parameters.ParamByName('@EventID').Value := AEvent.EventID;
    spGenerateBill.Parameters.ParamByName('@CustomerID').Value := AEvent.CustomerID;
    spGenerateBill.Parameters.ParamByName('@TimeOccurred').Value := AEvent.TimeOccurred;
    spGenerateBill.Parameters.ParamByName('@Units').Value  := AEvent.Units;
    spGenerateBill.Parameters.ParamByName('@Amount').Value := ARule.Amount;
    spGenerateBill.Parameters.ParamByName('@RuleID').Value := ARule.RuleID;

    spGenerateBill.Prepared := True;
    spGenerateBill.ExecProc;

    if spGenerateBill.Parameters.ParamByName('@ErrorCode').Value = 0 then
      Result := True;
  except
    on E: EDatabaseError do
      ShowMessage(e.Message + ' Parameters were: ' +
        spGenerateBill.Parameters.ToString);
  end;
end;

function TDataModuleBilling.RestageTestData: Boolean;
begin
  Result := False;
  try
    spRestageTestData.Close;
    spRestageTestData.Prepared := True;
    spRestageTestData.ExecProc;

    if spRestageTestData.Parameters.ParamByName('@ErrorCode').Value = 0 then
      Result := True;
  except
    on E: EDatabaseError do
      ShowMessage('Could not restage test data. The exception is: ' + e.Message);
  end;
end;

procedure TDataModuleBilling.tblCustomerRuleAfterPost(DataSet: TDataSet);
begin
  qryCustomerRules.Close;
  qryCustomerRules.Open;
end;

procedure TDataModuleBilling.tblRulesAfterPost(DataSet: TDataSet);
begin
  qryCustomerRules.Close;
  qryCustomerRules.Open;
end;

procedure TDataModuleBilling.tblUnbilledEventsAfterPost(DataSet: TDataSet);
begin
  qryUnbilledEvents.Close;
  qryUnbilledEvents.Open;
end;

procedure TDataModuleBilling.qryBilledEventsAfterScroll(DataSet: TDataSet);
begin
  FilterRulesByCustomer(qryBilledEvents.FieldByName('CustomerID').AsString);
end;

procedure TDataModuleBilling.qryUnbilledEventsAfterScroll(DataSet: TDataSet);
begin
  FilterRulesByCustomer(qryUnbilledEvents.FieldByName('CustomerID').AsString);
end;

procedure TDataModuleBilling.RefreshQueries;
begin
  qryUnbilledEvents.Close;
  qryUnbilledEvents.Open;
  qryBilledEvents.Close;
  qryBilledEvents.Open;
  qryCustomerRules.Close;
  qryCustomerRules.Open;
end;

procedure TDataModuleBilling.FilterRulesByCustomer(ACustomerID: string);
begin
  //Filter customer rule query so user can see rules for selected customer
  qryCustomerRules.Filtered := False;
  qryCustomerRules.Filter := 'CustomerID = ' + ACustomerID;
  qryCustomerRules.Filtered := True;
end;

end.


