{---------------------------------------------------------------------------------------------------
 Unit Name: TopBilling.dpr
 Author:    Janice Vann, vannjk@gmail.com
 Date:      04-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
program TopBilling;

uses
  Forms,
  MainBillViewForm in 'MainBillViewForm.pas' {formBillView},
  BillingDataModule in 'BillingDataModule.pas' {DataModuleBilling: TDataModule},
  EventMaintenanceForm in 'EventMaintenanceForm.pas' {formEventMaintenance},
  BillingRules in 'BillingRules.pas',
  RuleProcessor in 'RuleProcessor.pas',
  Events in 'Events.pas',
  RulesMaintenanceForm in 'RulesMaintenanceForm.pas' {FormRulesMaintenance},
  LoginForm in 'LoginForm.pas' {formLogin},
  msVCL in 'msVCL.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;

  Application.CreateForm(TDataModuleBilling, DataModuleBilling);
  if LoginForm.LogIn then begin
    Application.CreateForm(TformBillView, formBillView);
    Application.CreateForm(TformEventMaintenance, formEventMaintenance);
    Application.CreateForm(TFormRulesMaintenance, FormRulesMaintenance);
    Application.Run;
  end;
end.
