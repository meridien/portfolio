{---------------------------------------------------------------------------------------------------
 Unit Name: RuleProcessor
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit RuleProcessor;

interface

uses BillingRules, Events, Classes, DB, System.Generics.Collections;

type
  TRuleProcessor = class(TObject)
  private
    //Benefit of using a generic is avoiding TBillingRule typecast in ProcessUnbilledEvents method.
    BillingRules:  TObjectList<TBillingRule>;
    BillingEvents: TObjectList<TBillingEvent>;
    FRulesData:    TDataset;
    FEventsData:   TDataset;
    procedure LoadRules;
    procedure LoadEvents;
  public
    constructor Create(AOwner: TComponent; RulesData, EventsData: TDataset);
    destructor Destroy; override;
    procedure ProcessUnbilledEvents;
  end;

implementation

{ TRuleProcessor }

constructor TRuleProcessor.Create(AOwner: TComponent; RulesData, EventsData: TDataset);
begin
  BillingRules  := TObjectList<TBillingRule>.Create;
  BillingEvents := TObjectList<TBillingEvent>.Create;

  FRulesData  := RulesData;
  FEventsData := EventsData;

  LoadRules;
  LoadEvents;
end;

destructor TRuleProcessor.Destroy;
begin
  BillingRules.Clear;
  BillingRules.Free;
  BillingEvents.Clear;
  BillingEvents.Free;
  inherited;
end;

procedure TRuleProcessor.LoadEvents;
var
  iEventIdx: Integer;
begin
  BillingEvents.Clear;
  FEventsData.Close;
  FEventsData.Open;

  for iEventIdx := 0 to FEventsData.RecordCount - 1 do
  begin
    BillingEvents.Add(TBillingEvent.Create(FEventsData.FieldByName('EventID').AsInteger,
      FEventsData.FieldByName('CustomerID').AsInteger,
      FEventsData.FieldByName('TimeOccurred').AsDateTime,
      FEventsData.FieldByName('Units').AsFloat));
    FEventsData.Next;
  end;
end;

procedure TRuleProcessor.LoadRules;
var
  iRuleIdx: Integer;
begin
  BillingRules.Clear;
  FRulesData.Close;
  FRulesData.Open;

  for iRuleIdx := 0 to FRulesData.RecordCount - 1 do
  begin
    case FRulesData.FieldByName('RuleCategoryID').AsInteger of
      1: BillingRules.Add(TTimeBillingRule.Create(
          FRulesData.FieldByName('RuleID').AsInteger,
          FRulesData.FieldByName('RuleCategoryID').AsInteger,
          FRulesData.FieldByName('Priority').AsInteger,
          FRulesData.FieldByName('CustomerID').AsInteger,
          FRulesData.FieldByName('RuleDescription').AsString,
          FRulesData.FieldByName('RuleCategoryDescription').AsString,
          FRulesData.FieldByName('MinParam').AsString,
          FRulesData.FieldByName('MaxParam').AsString,
          FRulesData.FieldByName('Condition').AsString,
          FRulesData.FieldByName('UnitRate').AsCurrency,
          FRulesData.FieldByName('Discount').AsFloat));
      2: BillingRules.Add(TQuantityBillingRule.Create(
          FRulesData.FieldByName('RuleID').AsInteger,
          FRulesData.FieldByName('RuleCategoryID').AsInteger,
          FRulesData.FieldByName('Priority').AsInteger,
          FRulesData.FieldByName('CustomerID').AsInteger,
          FRulesData.FieldByName('RuleDescription').AsString,
          FRulesData.FieldByName('RuleCategoryDescription').AsString,
          FRulesData.FieldByName('MinParam').AsString,
          FRulesData.FieldByName('MaxParam').AsString,
          FRulesData.FieldByName('Condition').AsString,
          FRulesData.FieldByName('UnitRate').AsCurrency,
          FRulesData.FieldByName('Discount').AsFloat));
      3: BillingRules.Add(TDefaultBillingRule.Create(
          FRulesData.FieldByName('RuleID').AsInteger,
          FRulesData.FieldByName('RuleCategoryID').AsInteger,
          FRulesData.FieldByName('Priority').AsInteger,
          FRulesData.FieldByName('CustomerID').AsInteger,
          FRulesData.FieldByName('RuleDescription').AsString,
          FRulesData.FieldByName('RuleCategoryDescription').AsString,
          FRulesData.FieldByName('MinParam').AsString,
          FRulesData.FieldByName('MaxParam').AsString,
          FRulesData.FieldByName('Condition').AsString,
          FRulesData.FieldByName('UnitRate').AsCurrency,
          FRulesData.FieldByName('Discount').AsFloat));
    end;
    FRulesData.Next;
  end;
end;

procedure TRuleProcessor.ProcessUnbilledEvents;
var
  Event: TBillingEvent;
  Rule: TBillingRule;
begin
  for Event in BillingEvents do
  begin
    for Rule in BillingRules do
    begin
      if Rule.RuleIsValid(Event) then
      begin
        Rule.PerformRule(Event);
        Break;
      end;
    end;
  end;
end;

end.
