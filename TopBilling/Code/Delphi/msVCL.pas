{---------------------------------------------------------------------------------------------------
 Unit Name: msVCL
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit msVCL;

interface

uses
  System.SysUtils, Vcl.Controls;

type
  //Normally class helpers would be placed in a common location, never in a datamodule. Placing here for demo purposes.
  TControlHelper = class helper for TControl
    function IsTextEmpty: Boolean;
  end;

implementation

{ TControlHelper }
function TControlHelper.IsTextEmpty: Boolean;
begin
  Result := String(Self.Text).IsEmpty;
end;

end.
