object FormRulesMaintenance: TFormRulesMaintenance
  Left = 0
  Top = 0
  Caption = 'Maintain Billing Rules'
  ClientHeight = 720
  ClientWidth = 1042
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 1042
    Height = 504
    Align = alClient
    Caption = 'Build Rule'
    TabOrder = 0
    object RulesNavigator: TDBNavigator
      Left = 2
      Top = 477
      Width = 1038
      Height = 25
      DataSource = dsRules
      Align = alBottom
      TabOrder = 0
    end
    object RulesGrid: TDBGrid
      Left = 2
      Top = 15
      Width = 1038
      Height = 462
      Align = alClient
      DataSource = dsRules
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnColExit = RulesGridColExit
      OnDrawColumnCell = RulesGridDrawColumnCell
      OnKeyPress = RulesGridKeyPress
      Columns = <
        item
          Expanded = False
          FieldName = 'RuleID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RuleCategoryID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Description'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Priority'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnitRate'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Discount'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MinParam'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MaxParam'
          Visible = True
        end>
    end
    object RuleCategoryComboBox: TDBLookupComboBox
      Left = 76
      Top = 72
      Width = 406
      Height = 21
      DataField = 'RuleCategoryID'
      DataSource = dsRules
      KeyField = 'RuleCategoryID'
      ListField = 'Description'
      ListSource = dsRuleCategory
      TabOrder = 2
      Visible = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 504
    Width = 1042
    Height = 216
    Align = alBottom
    Caption = 'Associate Customer with Rule'
    TabOrder = 1
    ExplicitTop = 508
    object Label1: TLabel
      Left = 515
      Top = 29
      Width = 93
      Height = 13
      Caption = 'Selected RuleID is: '
    end
    object DBText1: TDBText
      Left = 614
      Top = 29
      Width = 65
      Height = 17
      DataField = 'RuleID'
      DataSource = dsRules
    end
    object DBText2: TDBText
      Left = 641
      Top = 52
      Width = 65
      Height = 17
      DataField = 'CustomerID'
      DataSource = dsCustomers
    end
    object Label2: TLabel
      Left = 517
      Top = 52
      Width = 118
      Height = 13
      Caption = 'Selected CustomerID is: '
    end
    object Label3: TLabel
      Left = 728
      Top = 13
      Width = 161
      Height = 13
      Caption = 'Customers associated with Rules:'
    end
    object DBGrid3: TDBGrid
      Left = 728
      Top = 32
      Width = 312
      Height = 182
      DataSource = dsCustomerRule
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CustomerID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RuleID'
          Visible = True
        end>
    end
    object Button1: TButton
      Left = 517
      Top = 87
      Width = 145
      Height = 57
      Caption = 'Add Customer Rule Association'
      TabOrder = 1
      WordWrap = True
      OnClick = Button1Click
    end
    object DBGrid1: TDBGrid
      Left = 2
      Top = 15
      Width = 480
      Height = 199
      Align = alLeft
      DataSource = dsCustomers
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CustomerID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FirstName'
          Width = 145
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LastName'
          Width = 183
          Visible = True
        end>
    end
  end
  object dsRuleCategory: TDataSource
    DataSet = DataModuleBilling.tblRuleCategory
    Left = 112
    Top = 144
  end
  object dsRules: TDataSource
    DataSet = DataModuleBilling.tblRules
    Left = 40
    Top = 144
  end
  object dsCustomers: TDataSource
    DataSet = DataModuleBilling.tblCustomers
    Left = 360
    Top = 536
  end
  object MainMenu1: TMainMenu
    Left = 1000
    Top = 8
    object File1: TMenuItem
      Caption = 'File'
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
  end
  object dsCustomerRule: TDataSource
    DataSet = DataModuleBilling.tblCustomerRule
    Left = 976
    Top = 544
  end
end
