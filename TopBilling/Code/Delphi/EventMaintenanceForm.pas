{---------------------------------------------------------------------------------------------------
 Unit Name: EventMaintenanceForm
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit EventMaintenanceForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BillingDataModule, StdCtrls, ExtCtrls, DBCtrls, Grids, DBGrids, DB,
  Menus;

type
  TformEventMaintenance = class(TForm)
    dsUnbilledEvents: TDataSource;
    DBGrid1:      TDBGrid;
    DBNavigator1: TDBNavigator;
    MainMenu1:    TMainMenu;
    File1:        TMenuItem;
    Exist1:       TMenuItem;
    procedure Exist1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formEventMaintenance: TformEventMaintenance;

implementation

{$R *.dfm}

procedure TformEventMaintenance.Exist1Click(Sender: TObject);
begin
  self.Close;
end;

procedure TformEventMaintenance.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DataModuleBilling.tblUnbilledEvents.Active := False;

end;

procedure TformEventMaintenance.FormShow(Sender: TObject);
begin
  if DataModuleBilling.tblUnbilledEvents.Active = False then
    DataModuleBilling.tblUnbilledEvents.Active := True;
end;

end.
