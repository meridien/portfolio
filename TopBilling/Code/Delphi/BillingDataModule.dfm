object DataModuleBilling: TDataModuleBilling
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 435
  Width = 599
  object ADOConnTopBilling: TADOConnection
    ConnectionString = 
      'Provider=SQLNCLI11.1;Password=S$ql2008;Persist Security Info=Tru' +
      'e;User ID=sa;Initial Catalog=TopBilling;Data Source=LOCALHOST'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 64
    Top = 40
  end
  object tblUnbilledEvents: TADOTable
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    AfterPost = tblUnbilledEventsAfterPost
    TableName = 'UnbilledEvents'
    Left = 192
    Top = 56
  end
  object qryUnbilledEvents: TADOQuery
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    AfterScroll = qryUnbilledEventsAfterScroll
    Parameters = <>
    SQL.Strings = (
      'select c.FirstName + '#39' '#39' + c.LastName as Customer, '
      '       e.Units, '
      '       e.TimeOccurred,'
      '       e.CustomerID,'
      '       e.EventID'
      'from UnbilledEvents e'
      '     join Customers c on c.CustomerID = e.CustomerID')
    Left = 288
    Top = 56
    object qryUnbilledEventsCustomer: TWideStringField
      FieldName = 'Customer'
      ReadOnly = True
      Size = 101
    end
    object qryUnbilledEventsUnits: TFloatField
      FieldName = 'Units'
    end
    object qryUnbilledEventsTimeOccurred: TDateTimeField
      FieldName = 'TimeOccurred'
      DisplayFormat = 'hh:mm am/pm'
    end
    object qryUnbilledEventsCustomerID: TSmallintField
      FieldName = 'CustomerID'
    end
    object qryUnbilledEventsEventID: TSmallintField
      FieldName = 'EventID'
    end
  end
  object qryBilledEvents: TADOQuery
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    AfterScroll = qryBilledEventsAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'select c.CustomerID, c.FirstName + '#39' '#39' + c.LastName as Customer,' +
        ' '
      '       be.Units, be.TimeOccurred, be.Amount, '
      
        '       r.Description, r.UnitRate, r.Discount, be.EventID, be.rul' +
        'eid'
      'from BilledEvents be'
      '     join Customers c on c.CustomerID = be.CustomerID'
      '     join Rules r on r.RuleID = be.RuleID')
    Left = 288
    Top = 120
    object qryBilledEventsCustomerID: TSmallintField
      FieldName = 'CustomerID'
    end
    object qryBilledEventsCustomer: TWideStringField
      FieldName = 'Customer'
      ReadOnly = True
      Size = 101
    end
    object qryBilledEventsUnits: TFloatField
      FieldName = 'Units'
    end
    object qryBilledEventsTimeOccurred: TDateTimeField
      FieldName = 'TimeOccurred'
      DisplayFormat = 'hh:mm am/pm'
    end
    object qryBilledEventsAmount: TBCDField
      FieldName = 'Amount'
      DisplayFormat = '$##.00'
      EditFormat = '$##.00'
      Precision = 19
    end
    object qryBilledEventsDescription: TWideStringField
      FieldName = 'Description'
      Size = 50
    end
    object qryBilledEventsUnitRate: TBCDField
      FieldName = 'UnitRate'
      EditFormat = '##.00'
      Precision = 19
    end
    object qryBilledEventsDiscount: TFloatField
      FieldName = 'Discount'
      EditFormat = '##.00'
    end
    object qryBilledEventsEventID: TSmallintField
      FieldName = 'EventID'
    end
    object qryBilledEventsruleid: TSmallintField
      FieldName = 'ruleid'
    end
  end
  object qryRules: TADOQuery
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT r.[RuleID]'
      '      ,r.[Priority]'
      '      ,cur.CustomerID'
      '      ,r.[RuleCategoryID]'
      '      ,rc.Description as RuleCategoryDescription'
      '      ,r.[UnitRate]'
      '      ,r.[Discount]'
      '      ,r.[MinParam]'
      '      ,r.[MaxParam]'
      '      ,r.[Description] as RuleDescription'
      '      ,rc.Condition'
      '  FROM [TopBilling].[dbo].[Rules] r'
      
        '  join [TopBilling].[dbo].[RuleCategory] rc on rc.RuleCategoryID' +
        ' = r.RuleCategoryID'
      
        '  left join [TopBilling].[dbo].[CustomerRule] cur on cur.RuleID ' +
        '= r.RuleID'
      '  order by cur.CustomerID, r.Priority desc')
    Left = 376
    Top = 56
  end
  object spGenerateBill: TADOStoredProc
    Connection = ADOConnTopBilling
    ProcedureName = 'GenerateBill;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 515
      end
      item
        Name = '@EventID'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@CustomerID'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@TimeOccurred'
        Attributes = [paNullable]
        DataType = ftDateTime
        Size = 16
        Value = Null
      end
      item
        Name = '@Units'
        Attributes = [paNullable]
        DataType = ftFloat
        Precision = 15
        Value = Null
      end
      item
        Name = '@Amount'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@RuleID'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@ErrorCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 515
      end>
    Left = 456
    Top = 56
  end
  object qryRuleCategory: TADOQuery
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [RuleCategoryID]'
      '      ,[Description]'
      '      ,[Condition]'
      '  FROM [TopBilling].[dbo].[RuleCategory]')
    Left = 288
    Top = 184
  end
  object tblRules: TADOTable
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    AfterPost = tblRulesAfterPost
    TableName = 'Rules'
    Left = 192
    Top = 120
    object tblRulesRuleID: TSmallintField
      FieldName = 'RuleID'
    end
    object tblRulesRuleCategoryID: TSmallintField
      FieldName = 'RuleCategoryID'
    end
    object tblRulesDescription: TWideStringField
      FieldName = 'Description'
      Size = 50
    end
    object tblRulesPriority: TIntegerField
      FieldName = 'Priority'
    end
    object tblRulesUnitRate: TBCDField
      FieldName = 'UnitRate'
      Precision = 19
    end
    object tblRulesDiscount: TFloatField
      FieldName = 'Discount'
    end
    object tblRulesMinParam: TWideStringField
      FieldName = 'MinParam'
      Size = 10
    end
    object tblRulesMaxParam: TWideStringField
      FieldName = 'MaxParam'
      Size = 10
    end
  end
  object tblRuleCategory: TADOTable
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    TableName = 'RuleCategory'
    Left = 192
    Top = 184
  end
  object tblCustomers: TADOTable
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    TableName = 'Customers'
    Left = 192
    Top = 248
    object tblCustomersCustomerID: TSmallintField
      FieldName = 'CustomerID'
    end
    object tblCustomersFirstName: TWideStringField
      FieldName = 'FirstName'
      Size = 50
    end
    object tblCustomersLastName: TWideStringField
      FieldName = 'LastName'
      Size = 50
    end
  end
  object qryCustomerRules: TADOQuery
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT cr.CustomerID'
      ',rules.[RuleID]'
      '      ,[Priority]'
      '      ,[RuleCategoryID]'
      '      ,[Description]'
      '      ,[UnitRate]'
      '      ,[Discount]'
      '      ,[MinParam]'
      '      ,[MaxParam]'
      '  FROM [TopBilling].[dbo].[Rules]'
      '  join CustomerRule cr on cr.RuleID = rules.ruleid'
      '  '
      '  --where cr.CustomerID = 102'
      '  order by Priority, cr.CustomerID desc')
    Left = 288
    Top = 232
    object qryCustomerRulesCustomerID: TSmallintField
      FieldName = 'CustomerID'
    end
    object qryCustomerRulesRuleID: TSmallintField
      FieldName = 'RuleID'
    end
    object qryCustomerRulesPriority: TIntegerField
      FieldName = 'Priority'
    end
    object qryCustomerRulesRuleCategoryID: TSmallintField
      FieldName = 'RuleCategoryID'
    end
    object qryCustomerRulesDescription: TWideStringField
      FieldName = 'Description'
      Size = 50
    end
    object qryCustomerRulesUnitRate: TBCDField
      FieldName = 'UnitRate'
      Precision = 19
    end
    object qryCustomerRulesDiscount: TFloatField
      FieldName = 'Discount'
    end
    object qryCustomerRulesMinParam: TWideStringField
      FieldName = 'MinParam'
      Size = 10
    end
    object qryCustomerRulesMaxParam: TWideStringField
      FieldName = 'MaxParam'
      Size = 10
    end
  end
  object spRestageTestData: TADOStoredProc
    Connection = ADOConnTopBilling
    ProcedureName = 'RestageTestData;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ErrorCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 464
    Top = 272
  end
  object tblCustomerRule: TADOTable
    Connection = ADOConnTopBilling
    CursorType = ctStatic
    AfterPost = tblCustomerRuleAfterPost
    TableName = 'CustomerRule'
    Left = 192
    Top = 312
  end
end
