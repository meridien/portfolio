{---------------------------------------------------------------------------------------------------
 Unit Name: BillingRules
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit BillingRules;

interface

uses Events, SysUtils, DateUtils, Dialogs;

type
  TBillingRule = class(TObject)
  private
    FRuleID:          integer;
    FRuleDescription: string;
    FRuleCategory:    integer;
    FRuleCategoryDescription: string;
    FCondition:       string;
    FPriority:        integer;
    FUnitRate:        currency;
    FDiscount:        double;
    FMinParam:        string;
    FMaxParam:        string;
    FCustomerID:      integer;
    FAmount:          currency;
  protected
  public
    constructor Create(ARuleID, ARuleCategory, APriority, ACustomerID: integer;
      ARuleDesc, ACatDesc, AMinParam, AMaxParam, ACondition: string; AUnitRate: currency;
      ADiscount: double);

    function RuleIsValid(Event: TBillingEvent): boolean; virtual; abstract;
    function PerformRule(Event: TBillingEvent): boolean; virtual;

    property RuleID: integer Read FRuleID Write FRuleID;
    property Priority: integer Read FPriority Write FPriority;
    property Amount: currency Read FAmount Write FAmount;
  end;

  TQuantityBillingRule = class(TBillingRule)
  public
    function RuleIsValid(Event: TBillingEvent): boolean; override;
  end;

  TTimeBillingRule = class(TBillingRule)
  public
    function RuleIsValid(Event: TBillingEvent): boolean; override;
  end;

  TDefaultBillingRule = class(TBillingRule)
  public
    function RuleIsValid(Event: TBillingEvent): boolean; override;
  end;


implementation

uses BillingDataModule;

{ TBillingRule }

constructor TBillingRule.Create(ARuleID, ARuleCategory, APriority,
  ACustomerID: integer;
  ARuleDesc, ACatDesc, AMinParam, AMaxParam,
  ACondition: string;
  AUnitRate: currency; ADiscount: double);
begin
  FRuleID := ARuleID;
  FRuleDescription := ARuleDesc;
  FRuleCategory := ARuleCategory;
  FRuleCategoryDescription := ACatDesc;
  FPriority := APriority;
  FUnitRate := AUnitRate;
  FDiscount := ADiscount;
  FMinParam := AMinParam;
  FMaxParam := AMaxParam;
  FCondition := ACondition;
  FCustomerID := ACustomerID;
end;

function TBillingRule.PerformRule(Event: TBillingEvent): boolean;
begin
  Result := False;

  try
    if FDiscount > 0 then
      FAmount := ((Event.Units * FUnitRate) * (1 - FDiscount))
    else
      FAmount := (Event.Units * FUnitRate);

    Result := DataModuleBilling.GenerateBill(Self, Event);
  except
    on e: Exception do
      ShowMessage(e.message);
  end;
end;

{ TQuantityBillingRule }

function TQuantityBillingRule.RuleIsValid(Event: TBillingEvent): boolean;
begin
  Result := False;

  if Event.CustomerID = FCustomerID then
  begin
    if ((Event.Units > StrToFloatDef(FMinParam, 0)) and
      (Event.Units < StrToFloatDef(FMaxParam, 0))) then
      Result := True;
  end;
end;

{ TTimeBillingRule }

function TTimeBillingRule.RuleIsValid(Event: TBillingEvent): boolean;
begin
  Result := False;

  try
    if Event.CustomerID = FCustomerID then
    begin
      if ((TimeOf(Event.TimeOccurred) >=
        TimeOf(StrToTime(FMinParam))) and
        (TimeOf(Event.TimeOccurred) <= TimeOf(StrToTime(FMaxParam)))) then
        Result := True;
    end;
  except
    Result := False;
  end;
end;

{ TDefaultBillingRule }

function TDefaultBillingRule.RuleIsValid(Event: TBillingEvent): boolean;
begin
  Result := False;

  //If default billing rule is reached for the customer, it is always valid
  if Event.CustomerID = FCustomerID then
    Result := True;
end;

end.

