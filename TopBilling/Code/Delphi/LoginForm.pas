{---------------------------------------------------------------------------------------------------
 Unit Name: LoginForm
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit LoginForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TformLogin = class(TForm)
    GroupBox1: TGroupBox;
    OKButton: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ServerNameEdit: TEdit;
    DatabaseNameEdit: TEdit;
    UserNameEdit: TEdit;
    PasswordEdit: TEdit;
    Button1: TButton;
    procedure OKButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ConnectionString: string;
  end;

var
  formLogin: TformLogin;

function LogIn: Boolean;

implementation

{$R *.dfm}

uses BillingDataModule, msVCL;

function LogIn: Boolean;
begin
  Result := False;
  with TformLogin.Create(nil) do
  try
    if ShowModal = mrOK then
      Result := True;
  finally
    Release;
  end;
end;

procedure TformLogin.OKButtonClick(Sender: TObject);
var
  ConnectionString: string;
begin
  ModalResult := mrNone;

  if ServerNameEdit.IsTextEmpty or DatabaseNameEdit.IsTextEmpty or
  UserNameEdit.IsTextEmpty or PasswordEdit.IsTextEmpty then
    ShowMessage('Please check that SQL Server login parameters are correct.')
  else begin
    ConnectionString := Format('Provider=SQLNCLI11.1;Password=%s;Persist Security Info=True;User ID=' +
       '%s;Initial Catalog=%s;Data Source=%s', [PasswordEdit.Text, UserNameEdit.Text, DatabaseNameEdit.Text, ServerNameEdit.Text]);
    if DataModuleBilling.Connect(ConnectionString) then
      ModalResult := mrOk
    else
      ShowMessage('Cannot connect with given login criteria. Please check your SQL Server login settings and try again.');
  end;
end;

end.
