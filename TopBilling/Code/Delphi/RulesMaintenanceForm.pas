{---------------------------------------------------------------------------------------------------
 Unit Name: RulesMaintenanceForm
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit RulesMaintenanceForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BillingDataModule, DB, StdCtrls, DBCtrls, Grids, DBGrids, ExtCtrls,
  Menus, Mask;

type
  TFormRulesMaintenance = class(TForm)
    GroupBox1:      TGroupBox;
    dsRuleCategory: TDataSource;
    dsRules:        TDataSource;
    dsCustomers:    TDataSource;
    MainMenu1:      TMainMenu;
    File1:          TMenuItem;
    Exit1:          TMenuItem;
    RulesNavigator: TDBNavigator;
    RulesGrid: TDBGrid;
    RuleCategoryComboBox: TDBLookupComboBox;
    GroupBox2: TGroupBox;
    DBGrid3: TDBGrid;
    Button1: TButton;
    Label1: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    dsCustomerRule: TDataSource;
    Label3: TLabel;
    procedure Exit1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure RulesGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer;
      Column: TColumn; State: TGridDrawState);
    procedure RulesGridColExit(Sender: TObject);
    procedure RulesGridKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormRulesMaintenance: TFormRulesMaintenance;

implementation

{$R *.dfm}

uses
  msVCL;

procedure TFormRulesMaintenance.Button1Click(Sender: TObject);
begin
  try
    if NOT DataModuleBilling.tblCustomerRule.Locate('CustomerID;RuleID',VarArrayOf([
       DataModuleBilling.tblCustomers.FieldByName('CustomerID').AsInteger,
       DataModuleBilling.tblRules.FieldByName('RuleID').AsInteger]),[]) then
    begin
      DataModuleBilling.tblCustomerRule.Append;
      DataModuleBilling.tblCustomerRule.FieldByName('CustomerID').AsInteger :=
         DataModuleBilling.tblCustomers.FieldByName('CustomerID').AsInteger;
      DataModuleBilling.tblCustomerRule.FieldByName('RuleID').AsInteger :=
         DataModuleBilling.tblRules.FieldByName('RuleID').AsInteger;
      DataModuleBilling.tblCustomerRule.Post;
    end
    else
      ShowMessage(Format('RuleID %d is already associated with CustomerID %d',[
         DataModuleBilling.tblRules.FieldByName('RuleID').AsInteger,
         DataModuleBilling.tblCustomers.FieldByName('CustomerID').AsInteger]));
  except
    on E: EDatabaseError do
      ShowMessage('Could not associate customer with rule. The exception is: ' + e.Message);
  end;
end;

procedure TFormRulesMaintenance.Exit1Click(Sender: TObject);
begin
  self.Close;
end;

procedure TFormRulesMaintenance.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DataModuleBilling.tblRules.Active := False;
  DataModuleBilling.tblRuleCategory.Active := False;
end;

procedure TFormRulesMaintenance.FormShow(Sender: TObject);
begin
  if DataModuleBilling.tblRuleCategory.Active = False then
    DataModuleBilling.tblRuleCategory.Active := True;

  if DataModuleBilling.tblRules.Active = False then
    DataModuleBilling.tblRules.Active := True;
end;

//In a hurry, so borrowed/adapted the embedded combobox in dbgrid logic from Zarko Gajic:
//http://delphi.about.com/od/usedbvcl/l/aa101403b.htm
procedure TFormRulesMaintenance.RulesGridColExit(Sender: TObject);
begin
  if RulesGrid.SelectedField.FieldName = RuleCategoryComboBox.DataField then
    RuleCategoryComboBox.Visible := False;
end;

procedure TFormRulesMaintenance.RulesGridDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (gdFocused in State) then
  begin
    if (Column.Field.FieldName = RuleCategoryComboBox.DataField) then begin
      RuleCategoryComboBox.Left := Rect.Left + RulesGrid.Left + 2;
      RuleCategoryComboBox.Top := Rect.Top + RulesGrid.Top + 2;
      RuleCategoryComboBox.Width := Rect.Right - Rect.Left;
      RuleCategoryComboBox.Width := Rect.Right - Rect.Left;
      RuleCategoryComboBox.Height := Rect.Bottom - Rect.Top;

      RuleCategoryComboBox.Visible := True;
    end;
  end
end;

procedure TFormRulesMaintenance.RulesGridKeyPress(Sender: TObject; var Key: Char);
begin
 if (key = Chr(9)) then Exit;//tab key

  if (RulesGrid.SelectedField.FieldName = RuleCategoryComboBox.DataField) then
  begin
    RuleCategoryComboBox.SetFocus;
    SendMessage(RuleCategoryComboBox.Handle, WM_Char, word(Key), 0);
  end
end;

end.
