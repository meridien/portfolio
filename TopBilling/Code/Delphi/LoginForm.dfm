object formLogin: TformLogin
  Left = 0
  Top = 0
  Caption = 'Login'
  ClientHeight = 264
  ClientWidth = 365
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 365
    Height = 217
    Align = alTop
    Caption = 'Please specify SQL Server login settings.'
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 36
      Width = 69
      Height = 13
      Caption = 'Server Name: '
    end
    object Label2: TLabel
      Left = 10
      Top = 78
      Width = 83
      Height = 13
      Caption = 'Database Name: '
    end
    object Label3: TLabel
      Left = 38
      Top = 121
      Width = 55
      Height = 13
      Caption = 'Username: '
    end
    object Label4: TLabel
      Left = 40
      Top = 164
      Width = 53
      Height = 13
      Caption = 'Password: '
    end
    object ServerNameEdit: TEdit
      Left = 93
      Top = 32
      Width = 265
      Height = 21
      TabOrder = 0
      Text = 'LOCALHOST'
    end
    object DatabaseNameEdit: TEdit
      Left = 93
      Top = 74
      Width = 265
      Height = 21
      TabOrder = 1
      Text = 'TopBilling'
    end
    object UserNameEdit: TEdit
      Left = 93
      Top = 117
      Width = 265
      Height = 21
      TabOrder = 2
    end
    object PasswordEdit: TEdit
      Left = 93
      Top = 160
      Width = 265
      Height = 21
      PasswordChar = '*'
      TabOrder = 3
    end
  end
  object OKButton: TButton
    Left = 98
    Top = 232
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKButtonClick
  end
  object Button1: TButton
    Left = 193
    Top = 232
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
