object formBillView: TformBillView
  Left = 0
  Top = 0
  Caption = 'TopBilling - Bill View'
  ClientHeight = 707
  ClientWidth = 1101
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 325
    Width = 1101
    Height = 5
    Cursor = crVSplit
    Align = alTop
    ExplicitTop = 368
    ExplicitWidth = 1097
  end
  object Panel1: TPanel
    Left = 0
    Top = 330
    Width = 1101
    Height = 334
    Align = alClient
    TabOrder = 0
    object GroupBox4: TGroupBox
      Left = 1
      Top = 1
      Width = 1099
      Height = 332
      Align = alClient
      Caption = 'Billed Events'
      TabOrder = 0
      object DBGrid1: TDBGrid
        Left = 2
        Top = 15
        Width = 1095
        Height = 315
        Align = alClient
        DataSource = dsBilledEvents
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CustomerID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Customer'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EventID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TimeOccurred'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Units'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnitRate'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Discount'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ruleid'
            Title.Caption = 'RuleID applied'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Description'
            Width = 230
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Amount'
            Title.Caption = 'Amount Billed'
            Width = 75
            Visible = True
          end>
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 664
    Width = 1101
    Height = 43
    Align = alBottom
    TabOrder = 1
    object btnRunBilling: TButton
      Left = 501
      Top = 4
      Width = 94
      Height = 36
      Caption = 'Run Billing'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btnRunBillingClick
    end
    object Button1: TButton
      Left = 960
      Top = 5
      Width = 115
      Height = 36
      Caption = 'Restage Test Data'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1101
    Height = 325
    Align = alTop
    TabOrder = 2
    object Splitter2: TSplitter
      Left = 425
      Top = 1
      Width = 5
      Height = 323
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 424
      Height = 323
      Align = alLeft
      Caption = 'Unbilled Events'
      TabOrder = 0
      object DBGrid2: TDBGrid
        Left = 2
        Top = 39
        Width = 420
        Height = 282
        Align = alBottom
        DataSource = dsUnbilledEvents
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CustomerID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Customer'
            Width = 89
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Units'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TimeOccurred'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EventID'
            Visible = True
          end>
      end
      object Button2: TButton
        Left = 280
        Top = 8
        Width = 138
        Height = 25
        Action = actMaintainUnbilledEvents
        TabOrder = 1
      end
    end
    object GroupBox3: TGroupBox
      Left = 430
      Top = 1
      Width = 670
      Height = 323
      Align = alClient
      Caption = 'Customer Rules'
      TabOrder = 1
      object DBGrid3: TDBGrid
        Left = 2
        Top = 39
        Width = 666
        Height = 282
        Align = alBottom
        DataSource = dsCustomerRules
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CustomerID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RuleID'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Priority'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Description'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UnitRate'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Discount'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MinParam'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MaxParam'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RuleCategoryID'
            Visible = True
          end>
      end
      object Button3: TButton
        Left = 312
        Top = 8
        Width = 139
        Height = 25
        Action = actMaintainRules
        TabOrder = 1
      end
    end
  end
  object dsUnbilledEvents: TDataSource
    DataSet = DataModuleBilling.qryUnbilledEvents
    Left = 352
    Top = 272
  end
  object MainMenu1: TMainMenu
    Left = 872
    object File1: TMenuItem
      Caption = 'File'
      object Exit1: TMenuItem
        Action = actExit
      end
    end
    object Edit1: TMenuItem
      Caption = 'Edit'
      object MaintainEvents1: TMenuItem
        Action = actMaintainUnbilledEvents
      end
      object MaintainRules1: TMenuItem
        Action = actMaintainRules
      end
    end
  end
  object ActionList1: TActionList
    Left = 912
    Top = 8
    object actExit: TAction
      Caption = 'Exit'
      OnExecute = actExitExecute
    end
    object actMaintainUnbilledEvents: TAction
      Caption = 'Add or Edit Unbilled Events'
      OnExecute = actMaintainUnbilledEventsExecute
    end
    object actMaintainRules: TAction
      Caption = 'Add or Edit Billing Rules'
      OnExecute = actMaintainRulesExecute
    end
  end
  object dsBilledEvents: TDataSource
    DataSet = DataModuleBilling.qryBilledEvents
    Left = 896
    Top = 504
  end
  object dsCustomerRules: TDataSource
    DataSet = DataModuleBilling.qryCustomerRules
    Left = 896
    Top = 272
  end
end
