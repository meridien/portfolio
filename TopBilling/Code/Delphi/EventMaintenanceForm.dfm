object formEventMaintenance: TformEventMaintenance
  Left = 0
  Top = 0
  Caption = 'Maintain Events'
  ClientHeight = 462
  ClientWidth = 764
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 764
    Height = 437
    Align = alClient
    DataSource = dsUnbilledEvents
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 0
    Top = 437
    Width = 764
    Height = 25
    DataSource = dsUnbilledEvents
    Align = alBottom
    TabOrder = 1
  end
  object dsUnbilledEvents: TDataSource
    DataSet = DataModuleBilling.tblUnbilledEvents
    Left = 672
    Top = 352
  end
  object MainMenu1: TMainMenu
    Left = 432
    Top = 8
    object File1: TMenuItem
      Caption = 'File'
      object Exist1: TMenuItem
        Caption = 'Exit'
        OnClick = Exist1Click
      end
    end
  end
end
