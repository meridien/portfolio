{---------------------------------------------------------------------------------------------------
 Unit Name: Events
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit Events;

interface

type
  TBillingEvent = class(TObject)
  private
    FEventID:      integer;
    FCustomerID:   integer;
    FTimeOccurred: TDateTime;
    FUnits:        double;
  public
    constructor Create(AEventID, ACustomerID: integer; ATimeOccurred: TDatetime; AUnits: double);
    property EventID: integer Read FEventID Write FEventID;
    property CustomerID: integer Read FCustomerID Write FCustomerID;
    property TimeOccurred: TDateTime Read FTimeOccurred Write FTimeOccurred;
    property Units: Double Read FUnits Write FUnits;
  end;

implementation

{ TBillingEvent }

constructor TBillingEvent.Create(AEventID, ACustomerID: integer; ATimeOccurred: TDatetime; AUnits:
   Double);
begin
  FEventID := AEventID;
  FCustomerID := ACustomerID;
  FTimeOccurred := ATimeOccurred;
  FUnits := AUnits;
end;


end.
