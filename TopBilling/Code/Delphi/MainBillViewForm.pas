{---------------------------------------------------------------------------------------------------
 Unit Name: BillViewForm
 Author:    Janice Vann, vannjk@gmail.com
 Date:      11-Feb-2015
 Purpose:   Sample project to demonstrate Delphi proficiency.
 History:   This sample project was first written in 2009 and has been updated to Delphi XE7 and
            MS SQL Server 2012. Basic requirements are recorded in the accompanying document,
            'SampleProjectRequirementsSummary.doc'.
 Credit:    Ownership rights are reserved. Upon request, use may be allowed with proper attribution.
---------------------------------------------------------------------------------------------------}
unit MainBillViewForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BillingDataModule, ActnList, Menus, StdCtrls, Grids, DBGrids,
  ExtCtrls, DBCtrls, DB, RuleProcessor, System.Actions, System.UITypes;

type
  TformBillView = class(TForm)
    dsUnbilledEvents: TDataSource;
    Panel1:          TPanel;
    MainMenu1:       TMainMenu;
    File1:           TMenuItem;
    Exit1:           TMenuItem;
    ActionList1:     TActionList;
    actExit:         TAction;
    GroupBox2:       TGroupBox;
    btnRunBilling:   TButton;
    dsBilledEvents:  TDataSource;
    Panel2:          TPanel;
    Edit1:           TMenuItem;
    actMaintainUnbilledEvents: TAction;
    MaintainEvents1: TMenuItem;
    MaintainRules1:  TMenuItem;
    actMaintainRules: TAction;
    dsCustomerRules: TDataSource;
    Button1: TButton;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    GroupBox3: TGroupBox;
    DBGrid3: TDBGrid;
    GroupBox4: TGroupBox;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Button2: TButton;
    Button3: TButton;
    procedure actExitExecute(Sender: TObject);
    procedure actMaintainUnbilledEventsExecute(Sender: TObject);
    procedure btnRunBillingClick(Sender: TObject);
    procedure actMaintainRulesExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    RuleProcessor: TRuleProcessor;
  end;

var
  formBillView: TformBillView;

implementation

{$R *.dfm}

uses EventMaintenanceForm, RulesMaintenanceForm;

procedure TformBillView.actExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TformBillView.actMaintainRulesExecute(Sender: TObject);
begin
  FormRulesMaintenance.ShowModal;
end;

procedure TformBillView.actMaintainUnbilledEventsExecute(Sender: TObject);
begin
  formEventMaintenance.ShowModal;
end;

procedure TformBillView.btnRunBillingClick(Sender: TObject);
begin
  RuleProcessor := TRuleProcessor.Create(self, DataModuleBilling.qryRules,
     DataModuleBilling.qryUnbilledEvents);
  try
    RuleProcessor.ProcessUnbilledEvents;
  finally
    RuleProcessor.Free;
  end;

  DataModuleBilling.RefreshQueries;
end;

procedure TformBillView.Button1Click(Sender: TObject);
begin
  if MessageDlg('Are you sure you want to restage test data? Any entered data will be lost.',
     mtWarning, [mbYes, mbNo],0, mbYes ) = mrYes then
       if DataModuleBilling.RestageTestData then
         DataModuleBilling.RefreshQueries;
end;

end.
