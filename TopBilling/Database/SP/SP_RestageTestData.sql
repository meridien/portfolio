if object_id('dbo.RestageTestData') is not null
  drop procedure dbo.RestageTestData
GO

CREATE PROCEDURE [dbo].[RestageTestData] 
	@ErrorCode int OUTPUT
AS
BEGIN
	DELETE from BilledEvents
	DELETE from UnbilledEvents
	DELETE from Customers
	DELETE from CustomerRule
	DELETE from Rules
	DELETE from RuleCategory

	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (1, 100, CAST(0x070040230E430000 AS Time), 8.5)
	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (2, 101, CAST(0x07007AA606C90000 AS Time), 6)
	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (3, 102, CAST(0x070080461C860000 AS Time), 12)
	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (4, 102, CAST(0x070050CFDF960000 AS Time), 3)
	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (5, 103, CAST(0x0700B893419F0000 AS Time), 2)
	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (6, 104, CAST(0x0700E03495640000 AS Time), 5)
	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (7, 105, CAST(0x070010ACD1530000 AS Time), 6)
	INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (8, 105, CAST(0x0700EC7572A30000 AS Time), 1.5)

	INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (100, N'John', N'Doe')
	INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (101, N'Bob', N'Smith')
	INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (102, N'William', N'Sears')
	INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (103, N'Anna', N'Watson')
	INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (104, N'Lana', N'Jones')
	INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (105, N'Jill', N'Burgess')

	INSERT [dbo].[RuleCategory] ([RuleCategoryID], [Description], [Condition]) VALUES (1, N'Time Rule', N'TimeOccurred > MinParam and TimeOccurred < MaxParam')
	INSERT [dbo].[RuleCategory] ([RuleCategoryID], [Description], [Condition]) VALUES (2, N'Quantity Rule', N'Units > MinParam AND Units < MaxParam')
	INSERT [dbo].[RuleCategory] ([RuleCategoryID], [Description], [Condition]) VALUES (3, N'Default Rule', N'True')

	INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (1, 1, N'TimeOccurred between 0:00 and 8:59', 1, 94.0000, NULL, N'0:00', N'8:59')
	INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (2, 1, N'TimeOccurred between 9:00 and 17:00', 2, 50.0000, NULL, N'9:00', N'17:00')
	INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (3, 1, N'TimeOccurred between 17:00 and 23:59', 3, 94.0000, 0, N'17:00', N'23:59')
	INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (4, 2, N'Units > 10 And Units < 20', 4, 45.0000, 0.1, N'10', N'20')
	INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (5, 2, N'Units > 20 And Units < 30', 5, 40.0000, 0.2, N'20', N'30')
	INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (6, 3, N'Default (when no other rule is true)', 0, 53.0000, NULL, NULL, NULL)

	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (100, 1)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (100, 2)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (100, 6)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (101, 3)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (101, 4)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (101, 6)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (102, 1)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (102, 6)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (103, 2)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (103, 6)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (104, 4)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (104, 6)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 1)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 2)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 3)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 4)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 5)
	INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 6)
			   
	Select @ErrorCode = @@Error;
	Return @ErrorCode			   
END
GO
