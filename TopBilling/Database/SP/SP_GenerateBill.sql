if object_id('dbo.GenerateBill') is not null
  drop procedure dbo.GenerateBill
GO

CREATE PROCEDURE [dbo].[GenerateBill] 
   	@EventID smallint,  
	@CustomerID smallint,
	@TimeOccurred time,
	@Units float,
	@Amount money,
	@RuleID smallint,
	@ErrorCode int OUTPUT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert Event with calculated amount into BilledEvents table
	INSERT INTO [dbo].[BilledEvents]
			   ([EventID]
			   ,[CustomerID]
			   ,[TimeOccurred]
			   ,[Units]
			   ,[Amount]
			   ,[RuleID])
		 VALUES
			   (@EventID
			   ,@CustomerID
			   ,@TimeOccurred
			   ,@Units
			   ,@Amount
			   ,@RuleID);
			   
	Select @ErrorCode = @@Error;
 	
	-- If BilledEvent successfully inserted, remove the now billed event from the UnbilledEvents table.
	If @ErrorCode = 0
	begin
		DELETE FROM UnbilledEvents WHERE EventID=@EventID;
	   
		Select @ErrorCode = @@Error;
	end
			   
	Return @ErrorCode			   
           
END
GO
