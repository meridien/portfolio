MS SQL Server Database for Sample billing project. 


There are two ways to create the initial database, either run the scripts manually in SSMS or modify the batch file for your environment and run it.

Options:
1) Run scripts manually.
  a) First, run TopBillingDB.sql to create the DB schema.
  b) Then run each .sql file under the \SP directory.

-OR-

2) Run the batch file.
  a) Rename "Recreate_TopBilling.RenameMe" to "Recreate_TopBilling.bat" and edit it.
  b) Modify the bat for your MSSQL environment settings for Server and sa password.
  c) Run it. Open SSMS and verify the DB is created.