/* Sample Project Database script. Updated to SQL 2012 */
USE [master]
GO

IF (NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = 'TopBilling'))
  CREATE DATABASE TopBilling;
GO

USE [TopBilling]
GO

/****** Object:  Table [dbo].[UnbilledEvents]  ******/
CREATE SEQUENCE Unbilled_Events_Seq
 AS SMALLINT
 START WITH 1
 INCREMENT BY 1;
GO

CREATE TABLE [dbo].[UnbilledEvents](
	[EventID] [smallint] DEFAULT NEXT VALUE FOR Unbilled_Events_Seq,
	[CustomerID] [smallint] NOT NULL,
	[TimeOccurred] DATETIME NOT NULL,
	[Units] [float] NOT NULL,
 CONSTRAINT [PK_UnbilledEvents] PRIMARY KEY CLUSTERED (EventID)
)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains billing events.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UnbilledEvents'
GO
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (1, 100, CAST(0x070040230E430000 AS Time), 8.5)
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (2, 101, CAST(0x07007AA606C90000 AS Time), 6)
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (3, 102, CAST(0x070080461C860000 AS Time), 12)
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (4, 102, CAST(0x070050CFDF960000 AS Time), 3)
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (5, 103, CAST(0x0700B893419F0000 AS Time), 2)
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (6, 104, CAST(0x0700E03495640000 AS Time), 5)
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (7, 105, CAST(0x070010ACD1530000 AS Time), 6)
INSERT [dbo].[UnbilledEvents] ([EventID], [CustomerID], [TimeOccurred], [Units]) VALUES (8, 105, CAST(0x0700EC7572A30000 AS Time), 1.5)


/****** Object:  Table [dbo].[Customers]  ******/
CREATE SEQUENCE Customers_Seq
 AS SMALLINT
 START WITH 100
 INCREMENT BY 1;
GO

CREATE TABLE [dbo].[Customers](
	[CustomerID] [smallint] DEFAULT NEXT VALUE FOR Customers_Seq,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED (CustomerID)
)
GO
INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (100, N'John', N'Doe')
INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (101, N'Bob', N'Smith')
INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (102, N'William', N'Sears')
INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (103, N'Anna', N'Watson')
INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (104, N'Lana', N'Jones')
INSERT [dbo].[Customers] ([CustomerID], [FirstName], [LastName]) VALUES (105, N'Jill', N'Burgess')


/****** Object:  Table [dbo].[Rule Category]  ******/
CREATE SEQUENCE Rule_Category_Seq
 AS SMALLINT
 START WITH 1
 INCREMENT BY 1;
GO

CREATE TABLE [dbo].[RuleCategory](
	[RuleCategoryID] [smallint] DEFAULT NEXT VALUE FOR Rule_Category_Seq,
	[Description] [nvarchar](50) NOT NULL,
	[Condition] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_RuleCategory] PRIMARY KEY CLUSTERED (RuleCategoryID)
)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes user defined rules.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RuleCategory'
GO
INSERT [dbo].[RuleCategory] ([RuleCategoryID], [Description], [Condition]) VALUES (1, N'Time Rule', N'TimeOccurred > MinParam and TimeOccurred < MaxParam')
INSERT [dbo].[RuleCategory] ([RuleCategoryID], [Description], [Condition]) VALUES (2, N'Quantity Rule', N'Units > MinParam AND Units < MaxParam')
INSERT [dbo].[RuleCategory] ([RuleCategoryID], [Description], [Condition]) VALUES (3, N'Default Rule', N'True')


/****** Object:  Table [dbo].[Rules] ******/
CREATE SEQUENCE Rules_Seq
 AS SMALLINT
 START WITH 1
 INCREMENT BY 1;
GO

CREATE TABLE [dbo].[Rules](
	[RuleID] [smallint] DEFAULT NEXT VALUE FOR Rules_Seq,
	[RuleCategoryID] [smallint] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Priority] [int] NOT NULL,
	[UnitRate] [money] NOT NULL,
	[Discount] [float] NULL,
	[MinParam] [nvarchar](10) NULL,
	[MaxParam] [nvarchar](10) NULL,
 CONSTRAINT [PK_Rules] PRIMARY KEY CLUSTERED (RuleID), 
 CONSTRAINT AK_Priority UNIQUE(Priority)
 )
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates order in which rules are executed (asc).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rules', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains rule data defined by user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rules'
GO
INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (1, 1, N'TimeOccurred between 0:00 and 8:59', 1, 94.0000, NULL, N'0:00', N'8:59')
INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (2, 1, N'TimeOccurred between 9:00 and 17:00', 2, 50.0000, NULL, N'9:00', N'17:00')
INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (3, 1, N'TimeOccurred between 17:00 and 23:59', 3, 94.0000, 0, N'17:00', N'23:59')
INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (4, 2, N'Units > 10 And Units < 20', 4, 45.0000, 0.1, N'10', N'20')
INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (5, 2, N'Units > 20 And Units < 30', 5, 40.0000, 0.2, N'20', N'30')
INSERT [dbo].[Rules] ([RuleID], [RuleCategoryID], [Description], [Priority], [UnitRate], [Discount], [MinParam], [MaxParam]) VALUES (6, 3, N'Default (when no other rule is true)', 0, 53.0000, NULL, NULL, NULL)


/****** Object:  Table [dbo].[CustomerRule] ******/
CREATE TABLE [dbo].[CustomerRule](
	[CustomerID] [smallint] NOT NULL,
	[RuleID] [smallint] NOT NULL,
 CONSTRAINT [PK_CustomerRule] PRIMARY KEY CLUSTERED (CustomerID, RuleID)
)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Associates customers with rules.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerRule'
GO
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (100, 1)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (100, 2)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (100, 6)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (101, 3)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (101, 4)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (101, 6)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (102, 1)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (102, 6)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (103, 2)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (103, 6)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (104, 4)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (104, 6)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 1)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 2)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 3)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 4)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 5)
INSERT [dbo].[CustomerRule] ([CustomerID], [RuleID]) VALUES (105, 6)


/****** Object:  Table [dbo].[BilledEvents] ******/
CREATE TABLE [dbo].[BilledEvents](
	[EventID] [smallint] NOT NULL,
	[CustomerID] [smallint] NOT NULL,
	[TimeOccurred] DATETIME NOT NULL,
	[Units] [float] NOT NULL,
	[Amount] [money] NULL,
	[RuleID] [smallint] NULL,
 CONSTRAINT [PK_BilledEvents] PRIMARY KEY CLUSTERED (EventID)
)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains events that have been billed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BilledEvents'
GO

/****** Object:  ForeignKey [FK_Rules_RuleCategory] ******/
ALTER TABLE [dbo].[Rules]  WITH CHECK ADD CONSTRAINT [FK_Rules_RuleCategory] FOREIGN KEY([RuleCategoryID])
REFERENCES [dbo].[RuleCategory] ([RuleCategoryID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Rules] CHECK CONSTRAINT [FK_Rules_RuleCategory]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ensure rule category exists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rules', @level2type=N'CONSTRAINT',@level2name=N'FK_Rules_RuleCategory'
GO

/****** Object:  ForeignKey [FK_CustomerRule_Customers] ******/
ALTER TABLE [dbo].[CustomerRule]  WITH CHECK ADD CONSTRAINT [FK_CustomerRule_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerRule] CHECK CONSTRAINT [FK_CustomerRule_Customers]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ensure customer exists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerRule', @level2type=N'CONSTRAINT',@level2name=N'FK_CustomerRule_Customers'
GO

/****** Object:  ForeignKey [FK_CustomerRule_Rules]  ******/
ALTER TABLE [dbo].[CustomerRule]  WITH CHECK ADD CONSTRAINT [FK_CustomerRule_Rules] FOREIGN KEY([RuleID])
REFERENCES [dbo].[Rules] ([RuleID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerRule] CHECK CONSTRAINT [FK_CustomerRule_Rules]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ensure Rule exists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerRule', @level2type=N'CONSTRAINT',@level2name=N'FK_CustomerRule_Rules'
GO


/****** Object:  ForeignKey [FK_BilledEvents_Rules]   ******/
ALTER TABLE [dbo].[BilledEvents]  WITH CHECK ADD CONSTRAINT [FK_BilledEvents_Rules] FOREIGN KEY([RuleID])
REFERENCES [dbo].[Rules] ([RuleID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BilledEvents] CHECK CONSTRAINT [FK_BilledEvents_Rules]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ensure specified rule exists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BilledEvents', @level2type=N'CONSTRAINT',@level2name=N'FK_BilledEvents_Rules'
GO
