# README #

### What is this repository for? ###

* This repository is made up of Janice Vann's work samples to demonstrate proficiencies in Software and Database Development and Documentation.

### How do I get set up? ###

* The TopBilling folder is a Delphi XE7 / SQL Server 2012 sample billing application. 
    * Take a look at SampleProjectRequirementsSummary.doc to get familiar with the requirements. 
    * Then to create the database and compile the application, follow the steps in SampleProjectDescription.doc.


### Who do I talk to? ###

* Janice Vann 
* vannjk@gmail.com